let runnigTotal = 0;
let buffer = "0";
let previousOperator = null;

const screen = document.querySelector('.screen');

function buttonClick(value) {
    if (isNaN(value)) {
        handleSymbol(value)
    } else {
        handleNumber(value);
    }
    screen.innerText = buffer;
}

function handleSymbol(symbol) {
    // console.log(symbol);
    switch (symbol) {
        case 'C':
            buffer = "0";
            runnigTotal = "0";
            break;
        case '=' :
            if(previousOperator == null)
                return;
            flushOperation(parseInt(buffer));
            previousOperator = null;
            buffer = runnigTotal;
            runnigTotal =0 ;
            break;
        case '←' :
            if(buffer.length == 1)
                buffer = '0';  
            else
                buffer = buffer.substring(0, buffer.length-1);
            break; 
        case '÷' :
        case '×' :
        case '-' :
        case '+' :
            handleMath(symbol);
            break;
    }
}

function handleMath(symbol) {
    if(buffer == "0")
        return;
    const buffer_int = parseInt(buffer);
    if(runnigTotal == 0)
        runnigTotal = buffer_int;
    else
        flushOperation(buffer_int);
    previousOperator = symbol;
    buffer = '0';
    // console.log(previousOperator);
}

function flushOperation(buffer_int) 
{
    switch (previousOperator) {
        case '÷' :
            runnigTotal /=buffer_int;
            break;
        case '×' :
            runnigTotal *=buffer_int;
            break;
        case '-' :
            runnigTotal -=buffer_int;
            break;
        case '+' :
            runnigTotal += buffer_int;
            break;
    }
}

function handleNumber(numberInString) {
    if (buffer == "0") {
        buffer = numberInString;
    } else {
        buffer = buffer + numberInString;
    }
}

function init() {
    document.querySelector('.calc-buttons')
        .addEventListener('click', function (event) {
            buttonClick(event.target.innerText);
        })
}

init();