async function apiFetcher(url, type) {
    var fetchedResult = await fetch(url, {
        method: type
    });
    // if (fetchedResult.ok == true)
    return (await fetchedResult.json());
}

async function displayCardsFunction() {
    const url = 'https://api.trello.com/1/lists/5e071d52f8af8e7817920291/cards?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14';
    var result = await apiFetcher(url, 'get');
    // console.log(result.length)
    result.forEach(element => {
        createDiv(element.name, element.id);
    });
}

async function displayCheckList(id, name) {
    document.getElementById('header-checklist').innerHTML = name;
    // console.log(name);
    const url = `https://api.trello.com/1/cards/${id}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    var result = await apiFetcher(url, 'get');
    // console.log(result);
    result.forEach(element => {
        createDivForChechlist(element.name, element.id);
        displaycheckItems(element.id,id);
    });
    document.querySelector('.add-checklist').addEventListener("click", function (e) {
        // event.stopPropagation();

        console.log(e.target.id);
        addChecklist(id);
    })
}

displayCardsFunction();

async function createDiv(input, id) {
    var div = document.createElement("div");
    div.id = id;
    // console.log(div);
    div.className = 'cards';
    // console.log(`createDiv ${div.id}`);
    div.addEventListener("click", event => {
        // console.log(event.target.id)
        document.getElementById('modal-card-content').innerHTML = " ";
        document.querySelector('.modal-footer').innerHTML = `<input type="text" id="text-input-checklist" placeholder="Create New Card">
        <button class="add-checklist" id = ${id}>Add Checklist</button>
        <button onclick="clrdiv()" type="button" class="btn btn-default cancel-button" data-dismiss="modal">
                  Cancel
                </button>`;
        displayCheckList(event.target.id, input);
    });
    div.setAttribute('data-target', '#myModal');
    div.setAttribute('data-toggle', 'modal');
    // document.querySelector('.add-checklst').
    div.innerHTML = `<h4>${input}</h4> <input type="button" class = "dlt-button" value="×" onclick="removeCard(this)" />`;
    document.getElementById("card-container").appendChild(div);
}

async function createDivForChechlist(input, id) {
    var div = document.createElement("div");
    div.id = id;
    div.className = 'checklist';
    // div.addEventListener('click')
    div.innerHTML = `${input} 
    <input type="button" class = "dlt-button" value="×" onclick="removeChecklist(this)" /><div class="checklistItems">
    <div class="checklistitems-body" id = ${id}>

    </div>
    <div class="checklistitems-footer" id = ${id}>
      <input type="text" id="${id}" placeholder="Create New Items">
      <button id = ${id} onclick = "addItemsFunction(id)">Add Items</button>
    </div>
  </div>
  `;

    document.getElementById("modal-card-content").appendChild(div);


}

async function removeCard(dltDiv) {
    event.stopPropagation();
    const url = `https://api.trello.com/1/cards/${dltDiv.parentNode.id}?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    await apiFetcher(url, 'delete');
    document.getElementById('card-container').removeChild(dltDiv.parentNode);
}

async function removeChecklist(dltDiv) {
    // console.log(dltDiv.parentNode);
    event.stopPropagation();
    const url = `https://api.trello.com/1/checklists/${dltDiv.parentNode.id}?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`
    await apiFetcher(url, 'delete');
    document.getElementById('modal-card-content').removeChild(dltDiv.parentNode);
}

async function addCardFunction() {
    console.log("add");
    const input = document.getElementById("text-input").value;
    const url = `https://api.trello.com/1/cards?name=${input}&idList=5e071d52f8af8e7817920291&keepFromSource=all&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    var result = await apiFetcher(url, 'post');
    createDiv(input, result.id);

    // console.log(result.id);
    // window.location.reload();
}

async function addChecklist(div_id) {
    // console.log(`addChkclst ${div_id}`);
    const input = document.getElementById("text-input-checklist").value;
    const url = `https://api.trello.com/1/checklists?idCard=${div_id}&name=${input}&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    var result = await apiFetcher(url, 'post');
    // console.log(`Checklis: ${result.id}`);
    createDivForChechlist(input, result.id);
}

function addItemsFunction(divdetails) {
    // console.log(divdetails.parentNode.id);
    const id = divdetails;
    console.log(id);
    const input = document.querySelector(`input[id="${id}"]`).value;
    const url = `https://api.trello.com/1/checklists/${id}/checkItems?name=${input}&pos=bottom&checked=false&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    var result = apiFetcher(url, 'post');
    createDivForCheckItems(input, result.id, id);
}

function createDivForCheckItems(name, checkItemId, chcklstId) {

    var div = document.createElement("div");
    div.id = checkItemId;
    div.className = 'checkitems';

    div.innerHTML = `<input type="checkbox" id=${checkItemId}/> <label for=${checkItemId}>${name}</label>`;
    // console.log(parentDiv.parentNode);
    document.getElementById(`${chcklstId}`).appendChild(div);
}

async function displaycheckItems(chcklstId, cardID) {
    console.log(chcklstId);
    const url = `https://api.trello.com/1/checklists/${chcklstId}/checkItems?key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`;
    const result = await apiFetcher(url, 'get');
    console.log(result);
    result.forEach(ele => {
        // console.log(ele.id);
        var div = document.createElement("div");
        div.id = ele.id;
        div.className = 'checkitems';

        div.innerHTML = `<input type="checkbox" class = "list${ele.id}" id=${ele.id}/> <label for=${ele.id}>${ele.name}</label>`;
        // console.log(parentDiv.parentNode);
        document.getElementById(`${ele.idChecklist}`).appendChild(div);
        document.querySelector(`.list${ele.id}`).addEventListener("click" ,function(event) {
            // checkItems(chcklstId, cardID, event.target);
            console.log(event);

        });
        // console.log(input);
        // createDivForCheckItems(ele.name, ele.id, ele.idChecklist)
    })
}

function clrdiv() {
    document.getElementById('modal-card-content').innerHTML = " ";
}

function checkItems(itemId, cardId, state) {
    console.log(state);
    // if (state == true) {
    //     state = 'complete';
    // } else {
    //     state = 'incomplete';
    // }
    // fetch(
    //     `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=0a888fcd467afb859a113e18472a165a&token=f287454275494e79765ee9355d8d4678edffe624889a85aa91fa254571b4bb14`, {
    //         method: 'PUT'
    //     }
    // );
}